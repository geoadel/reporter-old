/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */


import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-reporter',
  templateUrl: './about-reporter.component.html',
  styleUrls: ['./about-reporter.component.css']
})
export class AboutReporterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
